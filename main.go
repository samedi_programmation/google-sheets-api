package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/sheets/v4"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(tokFile)
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}

func main() {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, "https://www.googleapis.com/auth/spreadsheets")
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)

	srv, err := sheets.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Sheets client: %v", err)
	}

	// Prints the names and majors of students in a sample spreadsheet:
	// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit

	// Description de l'API : https://pkg.go.dev/google.golang.org/api/sheets/v4?tab=doc

	// Spreadsheet original avec les étudiants
	//spreadsheetId := "1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms"
	//readRange := "Class Data!A2:E"

	// Spreadsheet modifié : nouvelle colonne de cases à cocher
	spreadsheetId := "1rdAwtdCAIOaiBy-y62gSzgwDf-GMtavRnAGdJ96fyf0"
	readRange := "Class Data!A2:G"
	resp, err := srv.Spreadsheets.Values.Get(spreadsheetId, readRange).Do()
	if err != nil {
		log.Fatalf("Unable to retrieve data from sheet: %v", err)
	}

	spreadsheetId = "1H1dRYZxtO1BkdbZS_crBZ84BHN8D99ESM_d2EIXP874"
	rangeAddition := "Feuille 1!A2"

	if len(resp.Values) == 0 {
		fmt.Println("No data found.")
	} else {
		fmt.Println("Name, Major:")
		for _, row := range resp.Values {
			// Pour chaque ligne lue, on va la coller dans une nouvelle feuille

			// Print columns A and E, which correspond to indices 0 and 4.
			values := make([][]interface{}, 1)

			ligne := make([]interface{}, 3)
			ligne[0] = row[0]

			if row[6] == "TRUE" {
				ligne[2] = "LOLOL RECALÉ"
			} else {

				ligne[2] = "Bravo ! Bien joué !"
				//return
			}
			if row[4] == "Math" {
				ligne[1] = row[4]

				values[0] = ligne
				rowUpdated := addToSpreadsheet(srv, spreadsheetId, rangeAddition, values)

				fmt.Printf("%d cells updated\n", rowUpdated)
			} else {
				ligne[1] = "Not a Math fan"

				values[0] = ligne
				rowUpdated := addToSpreadsheet(srv, spreadsheetId, rangeAddition, values)

				fmt.Printf("%d cells updated\n", rowUpdated)

			}

			fmt.Printf("%s, %s\n", row[0], row[4])

		}

		clearFromSpreadsheet(srv, spreadsheetId, "A2")
		deleteFromSpreadsheet(srv, spreadsheetId, "B:C", "COLUMNS")
	}

}

// typeDeletion = ROWS or COLUMNS
func deleteFromSpreadsheet(srv *sheets.Service, spreadsheetId string, location string, typeDeletion string) {

	ctx := context.Background()

	rangeUpdate := make([]*sheets.ValueRange, 1)
	rangeUpdate[0] = &sheets.ValueRange{
		MajorDimension: typeDeletion,
		Range:          location,
	}

	batchUpdateValuesRequest := &sheets.BatchUpdateValuesRequest{
		Data:             rangeUpdate,
		ValueInputOption: "USER_ENTERED",
	}

	deleteResponse, err := srv.Spreadsheets.Values.BatchUpdate(spreadsheetId, batchUpdateValuesRequest).Context(ctx).Do()

	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("%d columns updated.\n%d rown updated.\n", deleteResponse.TotalUpdatedColumns, deleteResponse.TotalUpdatedRows)
	//{
	//	"deleteDimension": {
	//	"range": {
	//		"sheetId": sheetId,
	//			"dimension": "ROWS",
	//			"startIndex": 0,
	//			"endIndex": 3
	//	}
	//}
	//},
	//{
	//	"deleteDimension": {
	//	"range": {
	//		"sheetId": sheetId,
	//			"dimension": "COLUMNS",
	//			"startIndex": 1,
	//			"endIndex": 4
	//	}
	//}
	//},
}

func clearFromSpreadsheet(srv *sheets.Service, spreadsheetId string, location string) string {

	ctx := context.Background()

	clearRequest := &sheets.ClearValuesRequest{

	}

	deleteResponse, err := srv.Spreadsheets.Values.Clear(spreadsheetId, location, clearRequest).Context(ctx).Do()

	if err != nil {
		log.Fatal(err)
	}

	return deleteResponse.ClearedRange

}

func addToSpreadsheet(srv *sheets.Service, spreadsheetId string, location string, values [][]interface{}) int64 {

	rb := &sheets.ValueRange{
		Values: values,
	}

	ctx := context.Background()

	// How the input data should be interpreted.
	valueInputOption := "RAW" // TODO: Update placeholder value.

	// How the input data should be inserted.
	insertDataOption := "INSERT_ROWS" // TODO: Update placeholder value.

	appendResponse, err := srv.Spreadsheets.Values.Append(spreadsheetId, location, rb).ValueInputOption(valueInputOption).InsertDataOption(insertDataOption).Context(ctx).Do()

	if err != nil {
		log.Fatal(err)
	}

	return appendResponse.Updates.UpdatedCells

}
