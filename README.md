############################
# Les Samedi Programmation #
############################


Samedi 27/06/2020 : Interface avec l'API Google Sheets
======================================================


Ce code source permet de montrer comment s'interfacer avec l'API Google Sheets en Golang.

Liens de l'API : 
Exemple de démarrage & récupération des identifiants : https://developers.google.com/sheets/api/quickstart/go

Documentation : https://pkg.go.dev/google.golang.org/api/sheets/v4?tab=doc)



- Comment récupérer des données d'une feuille de calcul
- Comment écrire des données sur une feuille de calcul
- Comment conditionnellement écrire sur une feuille
- Comment vider des cellules

En cours de finition :
- Comment supprimer des lignes et des colonnes


Pour compiler et obtenir un exécutable, go build main.go

Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex